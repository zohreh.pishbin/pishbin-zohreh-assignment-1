import image1 from "./assets/city-skyline.jpg";
import image2 from "./assets/old-city-hall.jpg";
import image3 from "./assets/cn-tower.jpg";
import image4 from "./assets/rom.jpg";
import image5 from "./assets/rogers-center.jpg";
import image6 from "./assets/new-city-hall.jpg";
import SocialFollow from "./SocialFollow";
import "./App.css";
import { useState } from "react";

function App() {
  const images = [
    {
      title: "Toronto Sklyine",
      description: "Toronto Downtown Skyscrappers",
      image: image1,
    },
    {
      title: "Old City Hall",
      description: "Toronto Old City Hall",
      image: image2,
    },
    { title: "Cn Tower", description: "Canada Tallest Tower", image: image3 },
    { title: "ROM", description: "Royal Ontario Museum", image: image4 },
    {
      title: "Rogers Center",
      description: "Toronto Rogers Center",
      image: image5,
    },
    {
      title: "New City Hall",
      description: "Toronto New City Hall",
      image: image6,
    },
  ];
  const [selectedImage, setSelectedImage] = useState(null);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          The City of
          <b> Toronto</b>
        </p>
        <p className="Second-line">
          Historical Sites & <b> More</b>
        </p>
      </header>
        <section class="grid">
          {images.map((image) => (
            <img
              src={image.image}
              alt="Toronto Historical Buildings"
              onClick={() => setSelectedImage(image.image)}
            />
          ))}
        </section>
        <div
          id="overlay"
          style={{ visibility: selectedImage ? "visible" : "hidden" }}
        >
          <h1>
            <a class="close" onClick={() => setSelectedImage(null)}>
              X
            </a>
          </h1>
          <img src={selectedImage} />
        </div>
      
      <SocialFollow />
    </div>
  );
}

export default App;
