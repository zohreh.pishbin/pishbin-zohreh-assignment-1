import react from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faTwitter,
    faFacebook,
    faLinkedin,
    faInstagram,
} from "@fortawesome/free-brands-svg-icons";

export default function SocialFollow() {
    return (
        <div className="social-container"> 
            <a
            href="https://twitter.com/"
            className="twitter social"
            >
                <FontAwesomeIcon icon={faTwitter} size="1x" />
            </a>
            <a
            href="https://facebook.com/"
            className="facebook social"
            >
                <FontAwesomeIcon icon={faFacebook} size="1x" />
            </a>
            <a
            href="https://linkedin.com/"
            className="linkedin social"
            >
                <FontAwesomeIcon icon={faLinkedin} size="1x" />
            </a>
            <a
            href="https://instagram.com/"
            className="instagram social"
            >
                <FontAwesomeIcon icon={faInstagram} size="1x" />
            </a>
        </div>
    );
}